package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.ProfilePage;
import pageObjects.RegisterPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegisterTests extends BaseTest {

    @BeforeClass
    public void loginForRegister() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        loginPage.clickTab();
    }

    @DataProvider(name = "registerdp")
    public Iterator<Object[]> registerDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"Jon", "Doe", "jon.doe@gmail.com", "jondoe", "12345678", "12345678"});
        return dp.iterator();
    }

    @Test(dataProvider = "registerdp")
    public void registerPositiveTest(String firstname, String lastname, String email, String username, String password, String passConfirmation) {

        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.waitForRegisterPage();
        registerPage.register(firstname, lastname,email, username, password, passConfirmation);

        ProfilePage profilePage = new ProfilePage(driver);
        System.out.println("Registered user: " + profilePage.getUser());
        Assert.assertEquals(username, profilePage.getUser());

        profilePage.logOut();
    }
}
