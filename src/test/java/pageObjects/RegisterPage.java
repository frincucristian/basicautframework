package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "inputFirstName")
    WebElement firstnameInput;

    @FindBy(id = "inputLastName")
    WebElement lastnameInput;

    @FindBy(id = "inputEmail")
    WebElement emailInput;

    @FindBy(id = "inputUsername")
    WebElement usernameInput;

    @FindBy(id = "inputPassword")
    WebElement passInput;

    @FindBy(id = "inputPassword2")
    WebElement passConfInput;

    @FindBy(id = "register-submit")
    WebElement registerButton;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void register(String firstname, String lastname, String email, String username, String password, String passConfirmation) {
        firstnameInput.clear();
        firstnameInput.sendKeys(firstname);
        lastnameInput.clear();
        lastnameInput.sendKeys(lastname);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passInput.clear();
        passInput.sendKeys(password);
        passConfInput.clear();
        passConfInput.sendKeys(passConfirmation);
        registerButton.submit();
    }

    public void waitForRegisterPage() {
        wait.until(ExpectedConditions.elementToBeClickable(registerButton));
    }
}
